This module allows to automatically generate a thumbnail for an uploaded video.

It provides two additional field formatters for fields of type 'Video':

* __Thumbnail__: A thumbnail is generated and is displayed as an image.
* __HTML5 Video Player with thumbnail__: A thumbnail is generated and used as the "poster" inside the video player (meaning the thumbnail will be visible before the video is being played)
For each of these field formatters it's possible to fill in what frame should be used as the thumbnail (by indicating the amount of seconds from the start the frame is located) and to select an image style.

##Dependencies
The module depends on the [Video](https://www.drupal.org/project/video) module and the [PHP FFmpeg](https://www.drupal.org/project/php_ffmpeg) module.

##Installation
It is recommended to install this module using `composer`, since the [PHP FFmpeg](https://www.drupal.org/project/php_ffmpeg) module has a composer dependency on the PHP FFMpeg library (`php-ffmpeg/php-ffmpeg`).
The PHP FFMpeg library uses the `ffmpeg` and `ffprobe` CLI executables. These executables will need to be installed on your server.
Certain settings need to be configured for the [PHP FFmpeg](https://www.drupal.org/project/php_ffmpeg) module (e.g the paths to the `ffmpeg` and `ffprobe` CLI executables), so please consult their documentation for this.