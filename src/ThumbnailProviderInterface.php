<?php

namespace Drupal\local_video_thumbnail;

use Drupal\Core\Url;
use Drupal\file\Entity\File;

/**
 * Interface for the ThumbnailProvider class.
 *
 * @package Drupal\local_video_thumbnail
 */
interface ThumbnailProviderInterface {

  /**
   * Process the provided video file and retrieve/generate useful information about it.
   *
   * @param File $videoFile
   *    The video file.
   * @param $frameTime
   *    The time into the video to take the frame as thumbnail. This value is used when
   *    generating the data of the thumbnail. Including this value in the filename will
   *    make sure that a new thumbnail will be generated when the frame time is changed.
   */
  public function processVideo(File $videoFile, $frameTime);

  /**
   * Returns the video URI.
   *
   * @return string
   *    The video URI.
   */
  public function getVideoUri();

  /**
   * Returns the video URL.
   *
   * @return string
   *    The video URL.
   */
  public function getVideoUrl();

  /**
   * Returns the real path of the video.
   *
   * @return string
   *    The real path of the video.
   */
  public function getVideoRealPath();

  /**
   * Returns the video thumbnail filename.
   *
   * @return string
   *    The video thumbnail filename.
   */
  public function getVideoThumbnailFilename();

  /**
   * Returns the video thumbnail URI.
   *
   * @return string
   *    The video thumbnail URI.
   */
  public function getVideoThumbnailUri();

  /**
   * Returns the video thumbnail URL.
   *
   * @return string
   *    The video thumbnail URL.
   */
  public function getVideoThumbnailUrl();

  /**
   * Returns the real path of the video thumbnail.
   *
   * @return string
   *    The real path of the video thumbnail.
   */
  public function getVideoThumbnailRealPath();

  /**
   * Returns the URI of the video thumbnail in a requested image style.
   * (This function only returns a value if an image style thumbnail has
   *  been generated using 'generateThumbnailImageStyle()')
   *
   * @return NULL|string
   *    The URI  of the video thumbnail in the requested image style.
   */
  public function getVideoThumbnailImageStyleUri();

  /**
   * Returns the URL of the video thumbnail in a requested image style.
   * (This function only returns a value if an image style thumbnail has
   *  been generated using 'generateThumbnailImageStyle()')
   *
   * @return NULL|string
   *    The URL  of the video thumbnail in the requested image style.
   */
  public function getVideoThumbnailImageStyleUrl();

  /**
   * Returns the real path of the video thumbnail in a requested image style.
   * (This function only returns a value if an image style thumbnail has
   *  been generated using 'generateThumbnailImageStyle()')
   *
   * @return NULL|string
   *    The real path of the video thumbnail in the requested image style.
   */
  public function getVideoThumbnailImageStyleRealPath();

  /**
   * Returns the default settings.
   *
   * @return mixed
   *    The default settings.
   */
  public function getDefaultSettings();

  /**
   * Returns the settings summary.
   *
   * @param $frameTime
   *    The (default) value for the 'Frame time' field.
   * @param $imageStyle
   *    The (default) value for the 'Image style' field.
   * @return mixed
   *    The settings summary.
   */
  public function getSettingsSummary($frameTime, $imageStyle);

  /**
   * Returns the setting fields to configure the thumbnail settings.
   *
   * @param $frameTime
   *    The (default) value for the 'Frame time' field.
   * @param $imageStyle
   *    The (default) value for the 'Image style' field.
   * @return mixed
   *    The render arrays for the setting fields.
   */
  public function getSettingFields($frameTime, $imageStyle);

  /**
   * Generates a thumbnail of the video with the provided frame time.
   *
   * @param int $frameTime
   *    The time into the video to take the frame as thumbnail.
   */
  public function generateThumbnail($frameTime);

  /**
   * Generates the thumbnail of the video in a provided image style.
   *
   * @param $imageStyle
   *    The image style in which the thumbnail needs to be generated.
   */
  public function generateThumbnailImageStyle($imageStyle);

  /**
   * Creates the render array to display the thumbnail.
   *
   * @param Url|NULL $url
   *    The URL that needs to wrap the thumbnail. If no URL is provided, no URL will wrap the thumbnail.
   * @return array
   *    The render array.
   */
  public function renderThumbnail(Url $url = NULL);
}