<?php

namespace Drupal\local_video_thumbnail\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\local_video_thumbnail\ThumbnailProvider;
use Drupal\video\Plugin\Field\FieldFormatter\VideoPlayerFormatter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the local video player field formatter.
 *
 * @FieldFormatter(
 *   id = "local_video_player",
 *   label = @Translation("HTML5 Video Player with thumbnail"),
 *   field_types = {
 *     "video"
 *   }
 * )
 */

class Player extends VideoPlayerFormatter implements ContainerFactoryPluginInterface {

  /**
   * The thumbnail provider.
   *
   * @var ThumbnailProvider
   */
  protected $thumbnailProvider;

  /**
   * Constructs a new instance of the 'Player'-formatter.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param ThumbnailProvider $thumbnail_provider
   *    The thumbnail provider.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, AccountInterface $current_user, ThumbnailProvider $thumbnail_provider) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $current_user);
    $this->thumbnailProvider = $thumbnail_provider;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('current_user'),
      $container->get('local_video_thumbnail.thumbnail_provider')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $defaultSettings = parent::defaultSettings() + \Drupal::service('local_video_thumbnail.thumbnail_provider')->getDefaultSettings();
    $defaultSettings['download'] = '';
    return $defaultSettings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state) + $this->thumbnailProvider->getSettingFields($this->getSetting('frame_time'), $this->getSetting('image_style'));
    $element['download'] = [
        '#title' => $this->t('Allow download'),
        '#description' => $this->t('Allow the visitor to download the video file.'),
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('download'),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $arguments = $summary[0]->getArguments();
    $extraArguments = $this->thumbnailProvider->getSettingsSummary($this->getSetting('frame_time'), $this->getSetting('image_style'));
    $extraArguments['@download'] = $this->getSetting('download') ? $this->t('download allowed') : '';
    $argumentToAppend = array_keys($arguments)[count($arguments) - 1];
    foreach ($extraArguments as $extraArgumentKey => $extraArgument) {
      if (!empty($extraArgument))
        $extraArguments[$extraArgumentKey] = ', ' . $extraArgument;
    }
    $arguments += $extraArguments;
    $text = $summary[0]->getUntranslatedString();
    $text = str_replace($argumentToAppend, $argumentToAppend . implode('', array_keys($extraArguments)), $text);
    $summary[0] = $this->t($text, $arguments);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    $files = $this->getEntitiesToView($items, $langcode);
    foreach ($files as $delta => $file) {
      $this->thumbnailProvider->processVideo($file, $this->getSetting('frame_time'));
      // Generate thumbnail.
      if (file_exists($this->thumbnailProvider->getVideoRealPath()) && !file_exists($this->thumbnailProvider->getVideoThumbnailRealPath())) {
        $this->thumbnailProvider->generateThumbnail($this->getSetting('frame_time'));
      }
      // Generate thumbnail in image style.
      if ($imageStyle = $this->getSetting('image_style')) {
        $this->thumbnailProvider->generateThumbnailImageStyle($imageStyle);
      }
      $elements[$delta]['#theme'] = 'local_video_player';
      $elements[$delta]['#player_attributes']['thumbnail'] = $this->thumbnailProvider->getVideoThumbnailImageStyleUrl() ?: $this->thumbnailProvider->getVideoThumbnailUrl();
    }
    return $elements;
  }
}
