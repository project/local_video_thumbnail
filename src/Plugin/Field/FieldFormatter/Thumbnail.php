<?php

namespace Drupal\local_video_thumbnail\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Drupal\local_video_thumbnail\ThumbnailProvider;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the local video thumbnail field formatter.
 *
 * @FieldFormatter(
 *   id = "local_video_thumbnail",
 *   label = @Translation("Thumbnail"),
 *   field_types = {
 *     "video"
 *   }
 * )
 */
class Thumbnail extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The image style entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $imageStyleStorage;

  /**
   * The thumbnail provider.
   *
   * @var ThumbnailProvider
   */
  protected $thumbnailProvider;

  /**
   * Class constant for linking to content.
   */
  const LINK_CONTENT = 'content';

  /**
   * Class constant for linking to the file.
   */
  const LINK_FILE = 'file';

  /**
   * Constructs a new instance of the 'Thumbnail'-formatter.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Third party settings.
   * @param EntityStorageInterface $image_style_storage
   *    The image style storage.
   * @param ThumbnailProvider $thumbnail_provider
   *    The thumbnail provider.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, $settings, $label, $view_mode, $third_party_settings, EntityStorageInterface $image_style_storage, ThumbnailProvider $thumbnail_provider) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->imageStyleStorage = $image_style_storage;
    $this->thumbnailProvider = $thumbnail_provider;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')->getStorage('image_style'),
      $container->get('local_video_thumbnail.thumbnail_provider')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return \Drupal::service('local_video_thumbnail.thumbnail_provider')->getDefaultSettings() + ['link_image_to' => ''];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    $element = $element + $this->thumbnailProvider->getSettingFields($this->getSetting('frame_time'), $this->getSetting('image_style'));
    $element['link_image_to'] = [
      '#title' => $this->t('Link image to'),
      '#type' => 'select',
      '#empty_option' => $this->t('- None -'),
      '#default_value' => $this->getSetting('link_image_to'),
      '#options' => [
        static::LINK_CONTENT => $this->t('Content'),
        static::LINK_FILE => $this->t('File'),
      ],
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $arguments = $this->thumbnailProvider->getSettingsSummary($this->getSetting('frame_time'), $this->getSetting('image_style'));
    $arguments['@linked'] = $this->getSetting('link_image_to') ? $this->t('linked to ') . $this->getSetting('link_image_to') : '';
    $argumentKeys = array_keys($arguments);
    $argumentsText = implode('', $argumentKeys);
    foreach ($arguments as $argumentKey => $argument) {
      if ($argumentKey !== $argumentKeys[count($argumentKeys) - 1] && !empty($argument) && !empty(next($arguments)))
        $arguments[$argumentKey] = $argument . ', ';
    }
    $summary[] = $this->t("Video thumbnail ($argumentsText).", $arguments);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $value = $item->getValue();
      if (array_key_exists('target_id', $value)) {
        $file = File::load($item->getValue()['target_id']);
        $this->thumbnailProvider->processVideo($file, $this->getSetting('frame_time'));
        // Generate thumbnail.
        if (file_exists($this->thumbnailProvider->getVideoRealPath()) && !file_exists($this->thumbnailProvider->getVideoThumbnailRealPath())) {
          $this->thumbnailProvider->generateThumbnail($this->getSetting('frame_time'));
        }
        // Generate thumbnail in image style.
        if ($imageStyle = $this->getSetting('image_style')) {
          $this->thumbnailProvider->generateThumbnailImageStyle($imageStyle);
        }
        // Link image to.
        $url = NULL;
        if ($this->getSetting('link_image_to') == static::LINK_CONTENT) {
          $url = $items->getEntity()->toUrl();
        }
        elseif ($this->getSetting('link_image_to') == static::LINK_FILE) {
          $url = Url::fromUri($this->thumbnailProvider->getVideoUrl());
        }

        $elements[$delta] = $this->thumbnailProvider->renderThumbnail($url);
      }
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = parent::calculateDependencies();
    $styleId = $this->getSetting('image_style');
    if ($styleId && $style = ImageStyle::load($styleId)) {
      $dependencies[$style->getConfigDependencyKey()][] = $style->getConfigDependencyName();
    }
    return $dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function onDependencyRemoval(array $dependencies) {
    $changed = parent::onDependencyRemoval($dependencies);
    $styleId = $this->getSetting('image_style');
    if ($styleId && $style = ImageStyle::load($styleId)) {
      if (!empty($dependencies[$style->getConfigDependencyKey()][$style->getConfigDependencyName()])) {
        $replacementId = $this->imageStyleStorage->getReplacementId($styleId);
        // If a valid replacement has been provided in the storage, replace the
        // image style with the replacement and signal that the formatter plugin
        // settings were updated.
        if ($replacementId && ImageStyle::load($replacementId)) {
          $this->setSetting('image_style', $replacementId);
          $changed = TRUE;
        }
      }
    }
    return $changed;
  }

}
