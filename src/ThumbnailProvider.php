<?php

namespace Drupal\local_video_thumbnail;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\File\FileUrlGenerator;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\FFMpeg;

/**
 * Class ThumbnailProvider.
 *
 * @package Drupal\local_video_thumbnail
 */
class ThumbnailProvider implements ThumbnailProviderInterface {

  /**
   * The directory where thumbnails are stored.
   *
   * @var string
   */
  protected $thumbsDirectory = 'public://local-video-thumbnails';

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The file URL generator.
   *
   * @var FileUrlGenerator
   */
  protected $fileUrlGenerator;

  /**
   * The FFMpeg service.
   *
   * @var FFMpeg
   */
  protected $ffMpeg;

  /**
   * Class constant containing the machine name for the setting field 'Frame time'.
   */
  const FIELD_FRAME_TIME = 'frame_time';

  /**
   * Class constant containing the machine name for the setting field 'Image style'.
   */
  const FIELD_IMAGE_STYLE = 'image_style';

  /**
   * The video file.
   *
   * @var File
   */
  protected $videoFile;

  /**
   * The video URI.
   *
   * @var string
   */
  protected $videoUri;

  /**
   * The video URL.
   *
   * @var string
   */
  protected $videoUrl;

  /**
   * The real path of the video.
   *
   * @var string
   */
  protected $videoRealPath;

  /**
   * The file name for the thumbnail.
   *
   * @var string
   */
  protected $videoThumbnailFilename;

  /**
   * The video thumbnail URI.
   *
   * @var string
   */
  protected $videoThumbnailUri;

  /**
   * The video thumbnail URL.
   *
   * @var string
   */
  protected $videoThumbnailUrl;

  /**
   * The real path of the video thumbnail.
   *
   * @var string
   */
  protected $videoThumbnailRealPath;

  /**
   * The URI of the video thumbnail in the requested image style.
   *
   * @var string
   */
  protected $videoThumbnailImageStyleUri;

  /**
   * The URL of the video thumbnail in the requested image style.
   *
   * @var string
   */
  protected $videoThumbnailImageStyleUrl;

  /**
   * The real path of the video thumbnail in the requested image style.
   *
   * @var string
   */
  protected $videoThumbnailImageStyleRealPath;

  /**
   * Constructs a new instance of the provider.
   *
   * @param FileSystemInterface $fileSystem
   *    The file system.
   * @param FileUrlGenerator $fileUrlGenerator
   *   The file URL generator.
   * @param FFMpeg $ffMpeg
   *    The FFMpeg service.
   */
  public function __construct(FileSystemInterface $fileSystem, FileUrlGenerator $fileUrlGenerator, FFMpeg $ffMpeg) {
    $this->fileSystem = $fileSystem;
    $this->fileUrlGenerator = $fileUrlGenerator;
    $this->ffMpeg = $ffMpeg;
  }

  /**
   * {@inheritdoc}
   */
  public function processVideo(File $videoFile, $frameTime) {
    // Video file.
    $this->videoFile = $videoFile;
    // Video.
    $this->videoUri = $videoFile->getFileUri();
    $this->videoUrl = $this->fileUrlGenerator->generateAbsoluteString($this->videoUri);
    $this->videoRealPath = $this->fileSystem->realpath($this->videoUri);
    // Video thumbnail.
    $this->videoThumbnailFilename = str_replace(' ', '-', pathinfo($videoFile->getFilename(), PATHINFO_FILENAME));
    $this->videoThumbnailUri = $this->thumbsDirectory . '/' . $this->videoThumbnailFilename . '_' . $frameTime . '.png';
    $this->videoThumbnailUrl = $this->fileUrlGenerator->generateAbsoluteString($this->videoThumbnailUri);
    $this->videoThumbnailRealPath = $this->fileSystem->realpath($this->videoThumbnailUri);
  }

  /**
   * {@inheritdoc}
   */
  public function getVideoUri() {
    return $this->videoUri;
  }

  /**
   * {@inheritdoc}
   */
  public function getVideoUrl() {
    return $this->videoUrl;
  }

  /**
   * {@inheritdoc}
   */
  public function getVideoRealPath() {
    return $this->videoRealPath;
  }

  /**
   * {@inheritdoc}
   */
  public function getVideoThumbnailFilename() {
    return $this->videoThumbnailFilename;
  }

  /**
   * {@inheritdoc}
   */
  public function getVideoThumbnailUri() {
    return $this->videoThumbnailUri;
  }

  /**
   * {@inheritdoc}
   */
  public function getVideoThumbnailUrl() {
    return $this->videoThumbnailUrl;
  }

  /**
   * {@inheritdoc}
   */
  public function getVideoThumbnailRealPath() {
    return $this->videoThumbnailRealPath;
  }

  /**
   * {@inheritdoc}
   */
  public function getVideoThumbnailImageStyleUri() {
    return $this->videoThumbnailImageStyleUri;
  }

  /**
   * {@inheritdoc}
   */
  public function getVideoThumbnailImageStyleUrl() {
    return $this->videoThumbnailImageStyleUrl;
  }

  /**
   * {@inheritdoc}
   */
  public function getVideoThumbnailImageStyleRealPath() {
    return $this->videoThumbnailImageStyleRealPath;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultSettings() {
    return [
        static::FIELD_FRAME_TIME => '',
        static::FIELD_IMAGE_STYLE => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSettingsSummary($frameTime, $imageStyle) {
    $frameTime = $frameTime ?: 0;
    $imageStyle = $imageStyle ?: t('no image style');
    return [
        '@' . static::FIELD_FRAME_TIME => $frameTime . ' second(s)',
        '@' . static::FIELD_IMAGE_STYLE => $imageStyle,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSettingFields($frameTime, $imageStyle) {
    return [
      static::FIELD_FRAME_TIME => [
        '#title' => t('Frame time'),
        '#description' => t('Which frame should be used as the thumbnail?'),
        '#type' => 'number',
        '#default_value' => $frameTime ? $frameTime : 0,
        '#min' => 0,
        '#step' => 1,
        '#field_suffix' => 'second(s)',
        '#required' => TRUE,
      ],
      static::FIELD_IMAGE_STYLE => [
        '#title' => t('Image Style'),
        '#type' => 'select',
        '#default_value' => $imageStyle,
        '#required' => FALSE,
        '#options' => image_style_options(),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function generateThumbnail($frameTime = 0) {
    $this->fileSystem->prepareDirectory($this->thumbsDirectory, FileSystemInterface::CREATE_DIRECTORY);
    $video = $this->ffMpeg->open($this->videoUrl);
    $video->frame(TimeCode::fromSeconds($frameTime))->save($this->videoThumbnailRealPath);
  }

  /**
   * {@inheritdoc}
   */
  public function generateThumbnailImageStyle($imageStyle) {
    $imageStyle = ImageStyle::load($imageStyle);
    $this->videoThumbnailImageStyleUri = $imageStyle->buildUri($this->videoThumbnailUri);
    $this->videoThumbnailImageStyleUrl = $this->fileUrlGenerator->generateAbsoluteString($this->videoThumbnailImageStyleUri);
    $this->videoThumbnailImageStyleRealPath = $this->fileSystem->realpath($this->videoThumbnailImageStyleUri);
    if (!file_exists($this->videoThumbnailImageStyleRealPath)) {
      $imageStyle->createDerivative($this->videoThumbnailUri, $imageStyle->buildUri($this->videoThumbnailUri));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function renderThumbnail(Url $url = NULL) {
    $output = [
        '#theme' => 'image',
        '#uri' => $this->videoThumbnailImageStyleUri ?: $this->videoThumbnailUri,
    ];
    if ($url) {
      $output = [
          '#type' => 'link',
          '#title' => $output,
          '#url' => $url,
      ];
    }
    return $output;
  }

}
